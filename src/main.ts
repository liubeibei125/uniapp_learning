import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'
import Store from './store'
import './utils/mock'

Vue.config.productionTip = false
Vue.use(Vuex);
Vue.prototype.$store = Store

new App().$mount()
