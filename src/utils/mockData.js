import Mock from 'mockjs'

export default {
  getUserData: () => {
    return {
      code: 20000,
      data: {
        videoData: [
          {
            name: 'SpringBoot',
            value: Mock.Random.float(1000, 10000, 0, 3)
          },
          {
            name: 'iOS',
            value: Mock.Random.float(1000, 10000, 0, 3)
          },
          {
            name: 'php',
            value: Mock.Random.float(1000, 10000, 0, 3)
          },
          {
            name: 'h5',
            value: Mock.Random.float(1000, 10000, 0, 3)
          },
          {
            name: '小程序',
            value: Mock.Random.float(1000, 10000, 0, 3)
          }
        ]
      }
    }
  }
}
