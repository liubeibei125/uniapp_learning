import user from './user'
import im from './im'
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)


export default new Vuex.Store({
  modules: {
    user,   // 添加进模块中
    im
  }
})
