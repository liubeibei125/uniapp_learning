const state = {
  counts:1
}

// geeter 获取值
const getters = {
	counts: state => state.counts
}

// actions
const actions = {
  //  可以理解为在页面只调用actions的方法 不调用mutations的方法
  // actions主要负责请求接口 拿到请求的数据  然后调用commit函数触发mutations方法更改state值
  incrementAsync :async function({ commit, state }, payload) {
      // 请求接口数据（假的）
      // let data = await api.post('xxxxxx', payload)
      let data = {b: 10}
      commit('increment',data)     // 假装data = {b:10}
      return Promise.resolve(data)
  }
}
// mutations
const mutations = {
  increment:function (state, payload) {
  	 let b = payload.b
     b++
     state.counts = b
  }
}
export default {
  state,
  getters,
  actions,
  mutations
}
