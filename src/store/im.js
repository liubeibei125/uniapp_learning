import TIM from 'tim-wx-sdk';
import COS from "cos-wx-sdk-v5";
import genTestUserSig from  "../utils/GenerateTestUserSig"

const state = {
  tim:{},
  timReady:false,
  roomList: [
      {
        name: "A房",
        group: "A"
      },
      {
        name: "B房",
        group: "B"
      },
      {
        name: "C房",
        group: "C"
      }
  ],
  commentList:[]  // 聊天记录列表
}

const getters = {
	tim: state => state.tim,
  timReady: state => state.timReady,
  roomList: state => state.roomList,
  commentList: state => state.commentList
}

const mutations = {

}

const actions = {
  // 初始化聊天Im
  initTim: function ({ commit, dispatch, state }, payload) {
    // 每次进入app.vue都重新判断登录一下
    if(state.timReady === false) {
      let options = {
        SDKAppID: 1400278585  // 接入时需要将0替换为您的即时通信 IM 应用的 SDKAppID
      };

      state.tim = TIM.create(options); // SDK 实例通常用 tim 表示
      state.tim.setLogLevel(0); // 普通级别，日志量较多，接入时建议使用
      state.tim.registerPlugin({'cos-wx-sdk': COS});
      // 初始化聊天完成

      // 注册消息监听(单聊 群聊  系统通知包含加入群组离开群组等)
      let ON_MESSAGE_RECEIVED = function ({name, data}) {
        data.forEach((e, i) => {
          console.log(e)
          if(e.type === "TIMCustomElem") {  // 用户自定义消息
            payload = JSON.parse(e.payload.data)
            state.commentList.push({
              'name': payload.userName,
              'content': '进入直播间'
            })
          }else if (e === "TIMTextElem") {  // 文本消息
            state.commentList.push({
              'name': payload.userName,
              'content': e.payload.text
            })
          }
        })
      }
      state.tim.on(TIM.EVENT.MESSAGE_RECEIVED, ON_MESSAGE_RECEIVED);

      state.tim.on(TIM.EVENT.SDK_READY, function(data){  // 监听state redy状态
        state.timReady = true // tim准备好了
        // 模拟创建对应直播间的群组
        state.roomList.forEach((e, i) =>{
          // 创建im群组（腾讯云限制了群组人数为六千？如何突破）
          dispatch({
            type: 'initGroup',  // 表示触发的actions函数值
            name: e.name,
            group: e.group  // type之外的值都会被传递给payload对象
          })
        })
      })

      // 如果sdk报错或没有准备好 将tim的ready更改为未准备好（暂时不可用）
      let ERROR = function(data){
        state.timReady = false // tim异常了
      }
      state.tim.on(TIM.EVENT.ERROR, ERROR)
      state.tim.on(TIM.EVENT.SDK_NOT_READY, ERROR);

      // 网络状态变化监听
      state.tim.on(TIM.EVENT.NET_STATE_CHANGE, function(event) {
        // 网络状态发生改变（v2.5.0 起支持）。
        // event.name - TIM.EVENT.NET_STATE_CHANGE
        // event.data.state 当前网络状态，枚举值及说明如下：
        //   - TIM.TYPES.NET_STATE_CONNECTED - 已接入网络
        //   - TIM.TYPES.NET_STATE_CONNECTING - 连接中。很可能遇到网络抖动，SDK 在重试。接入侧可根据此状态提示“当前网络不稳定”或“连接中”
        //   - TIM.TYPES.NET_STATE_DISCONNECTED - 未接入网络。接入侧可根据此状态提示“当前网络不可用”。SDK 仍会继续重试，若用户网络恢复，SDK 会自动同步消息
      });


      // 开始登录
      const userID = uni.getStorageSync('userId');
      const userSig = genTestUserSig(userID+''); // 这个只是临时 上线要放在后端生成 登录的时候返回
      state.tim.login({userID: userID+'', userSig: userSig.userSig})
      return Promise.resolve({});
    }
  },
  // 发送消息
  sendMessage ({ commit, state }, payload) {
    let userId = uni.getStorageSync('userId');
    let userInfo = uni.getStorageSync('userInfo');
    let groupID = state.roomList[payload.index].group;
    let message = state.tim.createTextMessage({
      to: groupID,
      conversationType: TIM.TYPES.CONV_GROUP,
      payload: {
        text: payload.value
      }
    });
    let promise = state.tim.sendMessage(message);
    state.commentList.push({
      'name': userInfo.nickName,
      'content': payload.value
    })
  },
  // 进入直播房间的操作 判断加入群组
  joinRoom ({ commit, state }, payload) {
    let groupID = state.roomList[payload.index].group;
    let promise = state.tim.joinGroup({ groupID: groupID, type: TIM.TYPES.GRP_CHATROOM });
    let userId = uni.getStorageSync('userId');
    let userInfo = uni.getStorageSync('userInfo');

      // 消息优先级，用于群聊（v2.4.2起支持）。如果某个群的消息超过了频率限制，后台会优先下发高优先级的消息，详细请参考：https://cloud.tencent.com/document/product/269/3663#.E6.B6.88.E6.81.AF.E4.BC.98.E5.85.88.E7.BA.A7.E4.B8.8E.E9.A2.91.E7.8E.87.E6.8E.A7.E5.88.B6)
      // 支持的枚举值：TIM.TYPES.MSG_PRIORITY_HIGH, TIM.TYPES.MSG_PRIORITY_NORMAL（默认）, TIM.TYPES.MSG_PRIORITY_LOW, TIM.TYPES.MSG_PRIORITY_LOWEST
      // priority: TIM.TYPES.MSG_PRIORITY_HIGH,

    promise.then(function(imResponse) {
      let message = {}
      switch (imResponse.data.status) {
        case TIM.TYPES.JOIN_STATUS_SUCCESS: // 加群成功
          message = state.tim.createCustomMessage({
            to: groupID,
            conversationType: TIM.TYPES.CONV_GROUP,
            payload: {
              data: JSON.stringify({
                userID: userId,
                userName: userInfo.nickName,
                type: "join"
              }), // 用于标识该消息是骰子类型消息
              description: '加群成功', // 获取骰子点数
              extension: ''
              }
            });
            promise = state.tim.sendMessage(message);
            promise.then(() => {
              state.commentList.push({
                'name': userInfo.nickName,
                'content': '进入直播间'
              })
            })
          break;
        case TIM.TYPES.JOIN_STATUS_ALREADY_IN_GROUP: // 已经在群中
          message = state.tim.createCustomMessage({
            to: groupID,
            conversationType: TIM.TYPES.CONV_GROUP,
            payload: {
              data: JSON.stringify({
                userID: userId,
                userName: userInfo.nickName,
                type: "join"
              }), // 用于标识该消息是骰子类型消息
              description: '加群成功', // 获取骰子点数
              extension: ''
              }
          });
          promise = state.tim.sendMessage(message);
          promise.then(() => {
            state.commentList.push({
              'name': userInfo.nickName,
              'content': '进入直播间'
            })
          })
          break;
        default:
          break;
      }
    }).catch(function(imError){
      console.warn('joinGroup error:', imError); // 申请加群失败的相关信息
    });
  },
  // 创建群组 这个逻辑在直播端
  initGroup ({ commit, state }, payload) {
    let {name, group} = payload
    console.log(group)
    state.tim.createGroup({
      type: TIM.TYPES.GRP_CHATROOM,  // 聊天室
      name: name,  // 群组名字
      groupID: group  // 自定义群组id
    }).then((res) => {  // 创建成功
      console.log(res.data.group); // 创建的群的资料
    })
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
